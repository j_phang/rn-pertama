import React, { Component } from 'react'
import { Alert, StyleSheet, ImageBackground } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  Button,
  Left,
  Body,
  Icon,
  Text,
  Input,
  Item,
  Label
} from 'native-base'

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }
  componentDidMount() {

  }

  username = (event) => {
    this.setState({
      email: event.target.value
    })
  }

  password = (event) => {
    this.setState({
      password: event.target.value
    })
  }

  showAlert1 = () => {
    Alert.alert(
      'Log In',
      'Belum bisa login sayang',
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ]
    );
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Login Page Sayang</Title>
          </Body>
        </Header>
        {/* <ImageBackground source={{ uri: 'https://zeronarkobasmantri.files.wordpress.com/2012/06/farid.jpg' }} style={{ width: '100%', height: '100%' }}> */}
          <Content style={styles.Wrapper}>
            <Item floatingLabel style={styles.input}>

              <Label>
                <Text style={styles.text}>
                  Email Sayang
              </Text>
              </Label>
              <Input onChange={this.username} style={styles.inputField} />
            </Item>

            <Item floatingLabel style={styles.input}>
              <Label>
                <Text style={styles.text}>
                  Password Sayang
              </Text>
              </Label>
              <Input secureTextEntry={true} onChange={this.password} style={styles.inputField}/>
            </Item>
            <Item style={styles.anjing}>
            <Button rounded onPress={this.showAlert1} activeOpacity={.7} >
              <Icon name='people' />
              <Text style={styles.text}>
                Login
              </Text>
            </Button>
            </Item>
          </Content>

          <Footer >
            <Text style={styles.text}>Joe Wow</Text>
          </Footer>
        {/* </ImageBackground> */}
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  inputField: {
    width: 280,
    color: 'white',
    borderColor: 'white',
    marginTop: 5
  },
  Wrapper: {
    // display: "flex",
    // alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: '#1F3A93'
  },
  text: {
    color: 'white',
    fontSize: 20,
    display: "flex",
    justifyContent: "center",
    alignContent: "center"
  },
  input: {
    display: "flex",
    padding: 5,
    margin: 10,
    width: `100%`,
    justifyContent: "space-around"
  },
  label: {
    width: 80
  },
  tengah: {
    display: "flex",
    justifyContent: "center"
  },
  header: {
    backgroundColor: 'white'
  },
  anjing: {
    // width : '50%',
    paddingLeft : '25%',
    textDecorationLine : 'none'
  }
})
